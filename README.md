Captain Electric, LLC was started with the idea that customer service in the electrical industry would be revolutionized. We are licensed, bonded, and insured and specialize in generators, house rewires, and circuit breakers. Our work comes with a lifetime guarantee.

Address: 1510 West 400 South, #3, Orem, UT 84058, USA

Phone: 801-224-1779
